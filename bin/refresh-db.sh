#! /bin/sh

./bin/console doctrine:database:drop --force --if-exists
./bin/console doctrine:database:create
# ./bin/console doctrine:migration:migrate
./bin/console doctrine:schema:create
./bin/console doctrine:fixtures:load