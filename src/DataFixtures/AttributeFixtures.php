<?php

namespace App\DataFixtures;

use App\Entity\Attribute;
use App\Entity\AttributeValue;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AttributeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $i = 0;
        while ($i++ < 30) {
            $attr = new Attribute();
            $attr->setTitle('Attr - ' . $i);
            $attr->setName('attr' . $i);
            $manager->persist($attr);

            $j = 0;
            while ($j++ < 5) {
                $attrVal = new AttributeValue();
                $attrVal->setAttribute($attr);
                $attrVal->setValue('val-' . $i . '-' . $j);
                $manager->persist($attrVal);
            }
        }

        $manager->flush();
    }
}
