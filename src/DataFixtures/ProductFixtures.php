<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\AttributeValue;
use App\DataFixtures\AttributeFixtures;
use App\Repository\AttributeRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Repository\AttributeValueRepository;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    const PRODUCT_ATTR_RELATION = [
        0 => [1, 7, 12],
        1 => [2, 8, 13],
        2 => [3, 9, 14],
    ];

    protected $attrRepo;

    public function __construct(AttributeValueRepository $attrRepo)
    {
        $this->attrRepo = $attrRepo;
    }

    public function load(ObjectManager $manager)
    {
        $i = 0;
        while ($i++ < 30) {
            $product = new Product();
            $product->setTitle('product - ' . $i);
            $product->setDescription('product desc - ' . $i);
            $product->setPrice(rand(1000, 100000));
            $manager->persist($product);

            if (empty(static::PRODUCT_ATTR_RELATION[$i])) {
                continue;
            }

            $attrs = $this->attrRepo->findBy([
                'id' => static::PRODUCT_ATTR_RELATION[$i],
            ]);

            foreach ($attrs as $attr) {
                $product->addAttributeValue($attr);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AttributeFixtures::class
        ];
    }
}
