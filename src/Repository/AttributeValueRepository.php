<?php

namespace App\Repository;

use App\Entity\AttributeValue;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

use function Symfony\Component\DependencyInjection\Loader\Configurator\ref;

/**
 * @method AttributeValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method AttributeValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method AttributeValue[]    findAll()
 * @method AttributeValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttributeValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AttributeValue::class);
    }

    public function getListWithProductsCount(array $vals)
    {
        $query = $this->createQueryBuilder('av')
            ->join(
                'App:Product',
                'p',
                Join::WITH,
                'p member of av.products'
            );


        if (!empty($vals)) {
            $query->where('av.id in (:vals)')
                ->select([
                    'productAv.id',
                    'productAv.value',
                    'attr.name',
                    'attr.title',
                    'count(distinct p.id) as count',
                ])
                ->join('p.attributeValues', 'productAv')
                ->join('productAv.attribute', 'attr')
                ->setParameter('vals', $vals)
                ->groupBy('productAv')
                ;
        } else {
            $query
                ->select([
                    'av.id',
                    'av.value',
                    'attr.name',
                    'attr.title',
                    'count(p.id) as count',
                ])
                ->join('av.attribute', 'attr')
                ->groupBy('av');
        }

        return $query
            ->getQuery()
            ->getArrayResult();
    }
}
