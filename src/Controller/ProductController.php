<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\AttributeValueRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Cache\CacheInterface;

class ProductController extends AbstractController
{
    protected function getPaginatedProducts(array $params, int $page, int $itemsPerPage)
    {
        $productsQuery = $productRepo->getFilterProductsQuery($params);
        return $paginator->paginate(
            $productsQuery,
            $page,
            $itemsPerPage
        );
    }

    /**
     * @Route("/", name="homepage")
     */
    public function index(
        Request $request,
        ProductRepository $productRepo,
        PaginatorInterface $paginator,
        ContainerInterface $container,
        AttributeValueRepository $attrValRepo,
        CacheInterface $cache
    ): Response {
        $filterParams = !empty($request->get('filter'))
            ? array_merge(...array_values($request->get('filter')))
            : [];
        $page = $request->query->getInt('page', 1);
        $baseCacheKey = base64_encode(json_encode($filterParams));
        $itemsPerPage = $container->getParameter('items_per_page');

        $products = $cache->get(
            $baseCacheKey . '-products-' . $page,
            function () use ($page, $itemsPerPage, $filterParams, $productRepo, $paginator) {
            $productsQuery = $productRepo->getFilterProductsQuery($filterParams);

            return $paginator->paginate(
                $productsQuery,
                $page,
                $itemsPerPage
            );
        });

        $filterMap = $cache->get($baseCacheKey . '-filter', function () use ($attrValRepo, $filterParams) {
            $filters = $attrValRepo->getListWithProductsCount($filterParams);

            $filterMap = [];
            foreach ($filters as $filter) {
                if (empty($filterMap[$filter['name']])) {
                    $filterMap[$filter['name']] = [
                        'name' => $filter['name'],
                        'title' => $filter['title'],
                        'values' => [],
                    ];
                }

                $filterMap[$filter['name']]['values'][] = $filter;
            }

            return $filterMap;
        });

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'filters' => $filterMap,
            'checkedFilters' => !empty($filterParams) ? array_flip($filterParams) : [],
        ]);
    }
}
